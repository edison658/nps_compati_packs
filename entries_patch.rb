require 'csv'
require 'open-uri'

games = CSV.parse(open("http://nopaystation.com/tsv/PSV_GAMES.tsv"), { :col_sep => "\t", :headers => true })

filtered = games

result = ""
Dir["patch/PCS*/*.ppk"].each do |game|
	game_title_id = game.split("/")[1]
	game_name = filtered.find {|i| i["Title ID"] == game.split("/")[1] }["Name"]
	result += "#{game}=#{game_name}\n"
end

puts result
